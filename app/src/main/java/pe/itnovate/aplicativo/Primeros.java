package pe.itnovate.aplicativo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pe.itnovate.aplicativo.Models.Video;


public class Primeros extends AppCompatActivity {

    private ListView listadoVideos;
    private ArrayList<Video> videos = new ArrayList<>();

    String[] videitos = {"iFDOJB7ZFzg","iWQUDPYR-pA"};
    private videosAdapter adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primero);
        videos.add(new Video("Guía de primeros auxilios. 03 Soporte vital básico. ", R.drawable.video_6));
        videos.add(new Video("Fenomeno el Niño: Recomendaciones para prevenir enfermedade", R.drawable.video_5));

        adaptador = new videosAdapter(this,R.layout.itemvideolayout,videos);

        listadoVideos = (ListView)findViewById(R.id.listView);
        listadoVideos.setAdapter(adaptador);
        listadoVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Primeros.this, Auxilios.class);
                intent.putExtra("VIDEO_ID", videitos[position]);
                startActivity(intent);
            }
        });
    }



}
