package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Centro
{
  private Double Latitud;
  private String Direccion;
  private String Telefono;
  private java.util.Date updated;
  private Double Longitud;
  private java.util.Date created;
  private String Nombre;
  private String ownerId;
  private String objectId;
  public Double getLatitud()
  {
    return Latitud;
  }

  public void setLatitud( Double Latitud )
  {
    this.Latitud = Latitud;
  }

  public String getDireccion()
  {
    return Direccion;
  }

  public void setDireccion( String Direccion )
  {
    this.Direccion = Direccion;
  }

  public String getTelefono()
  {
    return Telefono;
  }

  public void setTelefono( String Telefono )
  {
    this.Telefono = Telefono;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public Double getLongitud()
  {
    return Longitud;
  }

  public void setLongitud( Double Longitud )
  {
    this.Longitud = Longitud;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getNombre()
  {
    return Nombre;
  }

  public void setNombre( String Nombre )
  {
    this.Nombre = Nombre;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

                                                    
  public Centro save()
  {
    return Backendless.Data.of( Centro.class ).save( this );
  }

  public Future<Centro> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Centro> future = new Future<Centro>();
      Backendless.Data.of( Centro.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Centro> callback )
  {
    Backendless.Data.of( Centro.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Centro.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Centro.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Centro.class ).remove( this, callback );
  }

  public static Centro findById( String id )
  {
    return Backendless.Data.of( Centro.class ).findById( id );
  }

  public static Future<Centro> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Centro> future = new Future<Centro>();
      Backendless.Data.of( Centro.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Centro> callback )
  {
    Backendless.Data.of( Centro.class ).findById( id, callback );
  }

  public static Centro findFirst()
  {
    return Backendless.Data.of( Centro.class ).findFirst();
  }

  public static Future<Centro> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Centro> future = new Future<Centro>();
      Backendless.Data.of( Centro.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Centro> callback )
  {
    Backendless.Data.of( Centro.class ).findFirst( callback );
  }

  public static Centro findLast()
  {
    return Backendless.Data.of( Centro.class ).findLast();
  }

  public static Future<Centro> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Centro> future = new Future<Centro>();
      Backendless.Data.of( Centro.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Centro> callback )
  {
    Backendless.Data.of( Centro.class ).findLast( callback );
  }

  public static BackendlessCollection<Centro> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Centro.class ).find( query );
  }

  public static Future<BackendlessCollection<Centro>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Centro>> future = new Future<BackendlessCollection<Centro>>();
      Backendless.Data.of( Centro.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Centro>> callback )
  {
    Backendless.Data.of( Centro.class ).find( query, callback );
  }
}