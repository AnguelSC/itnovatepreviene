package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Punto
{
  private java.util.Date created;
  private java.util.Date updated;
  private Double Latitud;
  private String ownerId;
  private Double Longitud;
  private String objectId;
  private Integer Orden;
  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public Double getLatitud()
  {
    return Latitud;
  }

  public void setLatitud( Double Latitud )
  {
    this.Latitud = Latitud;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public Double getLongitud()
  {
    return Longitud;
  }

  public void setLongitud( Double Longitud )
  {
    this.Longitud = Longitud;
  }

  public String getObjectId()
  {
    return objectId;
  }

                                                    
  public Punto save()
  {
    return Backendless.Data.of( Punto.class ).save( this );
  }

  public Future<Punto> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Punto> future = new Future<Punto>();
      Backendless.Data.of( Punto.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Punto> callback )
  {
    Backendless.Data.of( Punto.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Punto.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Punto.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Punto.class ).remove( this, callback );
  }

  public static Punto findById( String id )
  {
    return Backendless.Data.of( Punto.class ).findById( id );
  }

  public static Future<Punto> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Punto> future = new Future<Punto>();
      Backendless.Data.of( Punto.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Punto> callback )
  {
    Backendless.Data.of( Punto.class ).findById( id, callback );
  }

  public static Punto findFirst()
  {
    return Backendless.Data.of( Punto.class ).findFirst();
  }

  public static Future<Punto> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Punto> future = new Future<Punto>();
      Backendless.Data.of( Punto.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Punto> callback )
  {
    Backendless.Data.of( Punto.class ).findFirst( callback );
  }

  public static Punto findLast()
  {
    return Backendless.Data.of( Punto.class ).findLast();
  }

  public static Future<Punto> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Punto> future = new Future<Punto>();
      Backendless.Data.of( Punto.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Punto> callback )
  {
    Backendless.Data.of( Punto.class ).findLast( callback );
  }

  public static BackendlessCollection<Punto> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Punto.class ).find( query );
  }

  public static Future<BackendlessCollection<Punto>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Punto>> future = new Future<BackendlessCollection<Punto>>();
      Backendless.Data.of( Punto.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Punto>> callback )
  {
    Backendless.Data.of( Punto.class ).find( query, callback );
  }

  public Integer getOrden() {
    return Orden;
  }

  public void setOrden(Integer orden) {
    Orden = orden;
  }
}