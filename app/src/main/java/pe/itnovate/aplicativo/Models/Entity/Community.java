package pe.itnovate.aplicativo.Models.Entity;

import io.realm.RealmObject;

/**
 * Created by AnguelSC on 10/11/2015.
 */
public class Community extends RealmObject {
    private String name;
    private String owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
