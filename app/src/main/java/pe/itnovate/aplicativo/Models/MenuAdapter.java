package pe.itnovate.aplicativo.Models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pe.itnovate.aplicativo.MenuOptions;
import pe.itnovate.aplicativo.R;

/**
 * Created by AnguelSC on 20/11/2015.
 */
public class MenuAdapter extends ArrayAdapter<MenuOptions>{
    private LayoutInflater mInflater;
    private int mResource;

    public MenuAdapter(Context context, int resource, List<MenuOptions> objects) {
        super(context, resource, objects);

        mResource = resource;
        mInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView == null ? mInflater.inflate( mResource, parent, false ) : convertView;
        TextView nombre = (TextView) view.findViewById(R.id.textView2);
        ImageView image = (ImageView) view.findViewById(R.id.imageView2);
        MenuOptions menuOptions = getItem(position);
        nombre.setText(menuOptions.getTitle());
        image.setImageResource(menuOptions.getIcon());
        return view;
    }

}
