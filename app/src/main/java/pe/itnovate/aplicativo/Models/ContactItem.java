package pe.itnovate.aplicativo.Models;

import java.util.ArrayList;

/**
 * Created by AnguelSC on 14/11/2015.
 */
public class ContactItem {
    private String id;
    private String Name;
    private ArrayList<String> numbers = new ArrayList<>();

    public ContactItem(String _id,String _Name, ArrayList<String> _numbers){
        id = _id;
        Name = _Name;
        numbers = _numbers;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public void setNumbers(ArrayList<String> numbers) {
        this.numbers = numbers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
