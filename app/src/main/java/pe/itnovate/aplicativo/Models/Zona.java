package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Zona
{
  private String objectId;
  private java.util.Date created;
  private java.util.Date updated;
  private String ownerId;
  private String Descripcion;
  private java.util.List<Punto> Lugares;
  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getDescripcion()
  {
    return Descripcion;
  }

  public void setDescripcion( String Descripcion )
  {
    this.Descripcion = Descripcion;
  }

  public java.util.List<Punto> getLugares()
  {
    return Lugares;
  }

  public void setLugares( java.util.List<Punto> Lugares )
  {
    this.Lugares = Lugares;
  }

                                                    
  public Zona save()
  {
    return Backendless.Data.of( Zona.class ).save( this );
  }

  public Future<Zona> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Zona> future = new Future<Zona>();
      Backendless.Data.of( Zona.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Zona> callback )
  {
    Backendless.Data.of( Zona.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Zona.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Zona.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Zona.class ).remove( this, callback );
  }

  public static Zona findById( String id )
  {
    return Backendless.Data.of( Zona.class ).findById( id );
  }

  public static Future<Zona> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Zona> future = new Future<Zona>();
      Backendless.Data.of( Zona.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Zona> callback )
  {
    Backendless.Data.of( Zona.class ).findById( id, callback );
  }

  public static Zona findFirst()
  {
    return Backendless.Data.of( Zona.class ).findFirst();
  }

  public static Future<Zona> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Zona> future = new Future<Zona>();
      Backendless.Data.of( Zona.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Zona> callback )
  {
    Backendless.Data.of( Zona.class ).findFirst( callback );
  }

  public static Zona findLast()
  {
    return Backendless.Data.of( Zona.class ).findLast();
  }

  public static Future<Zona> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Zona> future = new Future<Zona>();
      Backendless.Data.of( Zona.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Zona> callback )
  {
    Backendless.Data.of( Zona.class ).findLast( callback );
  }

  public static BackendlessCollection<Zona> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Zona.class ).find( query );
  }

  public static Future<BackendlessCollection<Zona>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Zona>> future = new Future<BackendlessCollection<Zona>>();
      Backendless.Data.of( Zona.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Zona>> callback )
  {
    Backendless.Data.of( Zona.class ).find( query, callback );
  }
}