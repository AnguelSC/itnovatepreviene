package pe.itnovate.aplicativo.Models;

/**
 * Created by Angel Sirlopu C on 18/10/2015.
 */
public class Video {
    private String Link;
    private String Descripcion;
    private int Imagen;

    public String getLink() {
        return Link;
    }

    public Video(String descripcion, int imagen) {
        Descripcion = descripcion;
        Imagen = imagen;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public int getImagen() {
        return Imagen;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }
}
