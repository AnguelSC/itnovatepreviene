package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Alerta
{
  private java.util.Date created;
  private Double Longitud;
  private java.util.Date updated;
  private Integer Estado;
  private String Nombre;
  private String ownerId;
  private String Telefono;
  private Double Latitud;
  private String objectId;
  public java.util.Date getCreated()
  {
    return created;
  }

  public Double getLongitud()
  {
    return Longitud;
  }

  public void setLongitud( Double Longitud )
  {
    this.Longitud = Longitud;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public Integer getEstado()
  {
    return Estado;
  }

  public void setEstado( Integer Estado )
  {
    this.Estado = Estado;
  }

  public String getNombre()
  {
    return Nombre;
  }

  public void setNombre( String Nombre )
  {
    this.Nombre = Nombre;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getTelefono()
  {
    return Telefono;
  }

  public void setTelefono( String Telefono )
  {
    this.Telefono = Telefono;
  }

  public Double getLatitud()
  {
    return Latitud;
  }

  public void setLatitud( Double Latitud )
  {
    this.Latitud = Latitud;
  }

  public String getObjectId()
  {
    return objectId;
  }

                                                    
  public Alerta save()
  {
    return Backendless.Data.of( Alerta.class ).save( this );
  }

  public Future<Alerta> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Alerta> future = new Future<Alerta>();
      Backendless.Data.of( Alerta.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Alerta> callback )
  {
    Backendless.Data.of( Alerta.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Alerta.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Alerta.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Alerta.class ).remove( this, callback );
  }

  public static Alerta findById( String id )
  {
    return Backendless.Data.of( Alerta.class ).findById( id );
  }

  public static Future<Alerta> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Alerta> future = new Future<Alerta>();
      Backendless.Data.of( Alerta.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Alerta> callback )
  {
    Backendless.Data.of( Alerta.class ).findById( id, callback );
  }

  public static Alerta findFirst()
  {
    return Backendless.Data.of( Alerta.class ).findFirst();
  }

  public static Future<Alerta> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Alerta> future = new Future<Alerta>();
      Backendless.Data.of( Alerta.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Alerta> callback )
  {
    Backendless.Data.of( Alerta.class ).findFirst( callback );
  }

  public static Alerta findLast()
  {
    return Backendless.Data.of( Alerta.class ).findLast();
  }

  public static Future<Alerta> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Alerta> future = new Future<Alerta>();
      Backendless.Data.of( Alerta.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Alerta> callback )
  {
    Backendless.Data.of( Alerta.class ).findLast( callback );
  }

  public static BackendlessCollection<Alerta> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Alerta.class ).find( query );
  }

  public static Future<BackendlessCollection<Alerta>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Alerta>> future = new Future<BackendlessCollection<Alerta>>();
      Backendless.Data.of( Alerta.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Alerta>> callback )
  {
    Backendless.Data.of( Alerta.class ).find( query, callback );
  }
}