package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Colegio
{
  private String objectId;
  private String Direccion;
  private java.util.Date created;
  private String Nombre;
  private String ownerId;
  private java.util.Date updated;
  private Double Longitud;
  private Double Latitud;
  private String Telefono;
  public String getObjectId()
  {
    return objectId;
  }

  public String getDireccion()
  {
    return Direccion;
  }

  public void setDireccion( String Direccion )
  {
    this.Direccion = Direccion;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getNombre()
  {
    return Nombre;
  }

  public void setNombre( String Nombre )
  {
    this.Nombre = Nombre;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public Double getLongitud()
  {
    return Longitud;
  }

  public void setLongitud( Double Longitud )
  {
    this.Longitud = Longitud;
  }

  public Double getLatitud()
  {
    return Latitud;
  }

  public void setLatitud( Double Latitud )
  {
    this.Latitud = Latitud;
  }

  public String getTelefono()
  {
    return Telefono;
  }

  public void setTelefono( String Telefono )
  {
    this.Telefono = Telefono;
  }

                                                    
  public Colegio save()
  {
    return Backendless.Data.of( Colegio.class ).save( this );
  }

  public Future<Colegio> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Colegio> future = new Future<Colegio>();
      Backendless.Data.of( Colegio.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Colegio> callback )
  {
    Backendless.Data.of( Colegio.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Colegio.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Colegio.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Colegio.class ).remove( this, callback );
  }

  public static Colegio findById( String id )
  {
    return Backendless.Data.of( Colegio.class ).findById( id );
  }

  public static Future<Colegio> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Colegio> future = new Future<Colegio>();
      Backendless.Data.of( Colegio.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Colegio> callback )
  {
    Backendless.Data.of( Colegio.class ).findById( id, callback );
  }

  public static Colegio findFirst()
  {
    return Backendless.Data.of( Colegio.class ).findFirst();
  }

  public static Future<Colegio> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Colegio> future = new Future<Colegio>();
      Backendless.Data.of( Colegio.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Colegio> callback )
  {
    Backendless.Data.of( Colegio.class ).findFirst( callback );
  }

  public static Colegio findLast()
  {
    return Backendless.Data.of( Colegio.class ).findLast();
  }

  public static Future<Colegio> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Colegio> future = new Future<Colegio>();
      Backendless.Data.of( Colegio.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Colegio> callback )
  {
    Backendless.Data.of( Colegio.class ).findLast( callback );
  }

  public static BackendlessCollection<Colegio> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Colegio.class ).find( query );
  }

  public static Future<BackendlessCollection<Colegio>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Colegio>> future = new Future<BackendlessCollection<Colegio>>();
      Backendless.Data.of( Colegio.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Colegio>> callback )
  {
    Backendless.Data.of( Colegio.class ).find( query, callback );
  }
}