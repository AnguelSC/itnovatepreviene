package pe.itnovate.aplicativo.Models.Entity;

import io.realm.RealmObject;

/**
 * Created by AnguelSC on 14/11/2015.
 */
public class Member extends RealmObject {
    private String name;
    private String numberPhone;
    private Community community;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Community getCommunity() {
        return community;
    }

    public void setCommunity(Community community) {
        this.community = community;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
