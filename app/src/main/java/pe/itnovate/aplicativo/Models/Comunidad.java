package pe.itnovate.aplicativo.Models;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import pe.itnovate.aplicativo.Future;

public class Comunidad
{
    private String ownerId;
    private java.util.Date created;
    private String Nombre;
    private String objectId;
    private java.util.Date updated;
    private BackendlessUser propietario;
    private java.util.List<BackendlessUser> Miembros;
    public String getOwnerId()
    {
        return ownerId;
    }

    public java.util.Date getCreated()
    {
        return created;
    }

    public String getNombre()
    {
        return Nombre;
    }

    public void setNombre( String Nombre )
    {
        this.Nombre = Nombre;
    }

    public String getObjectId()
    {
        return objectId;
    }

    public java.util.Date getUpdated()
    {
        return updated;
    }

    public BackendlessUser getPropietario()
    {
        return propietario;
    }

    public void setPropietario( BackendlessUser Propietario )
    {
        this.propietario = Propietario;
    }

    public java.util.List<BackendlessUser> getMiembros()
    {
        return Miembros;
    }

    public void setMiembros( java.util.List<BackendlessUser> Miembros )
    {
        this.Miembros = Miembros;
    }


    public Comunidad save()
    {
        return Backendless.Data.of( Comunidad.class ).save( this );
    }

    public Future<Comunidad> saveAsync()
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<Comunidad> future = new Future<Comunidad>();
            Backendless.Data.of( Comunidad.class ).save( this, future );

            return future;
        }
    }

    public void saveAsync( AsyncCallback<Comunidad> callback )
    {
        Backendless.Data.of( Comunidad.class ).save( this, callback );
    }

    public Long remove()
    {
        return Backendless.Data.of( Comunidad.class ).remove( this );
    }

    public Future<Long> removeAsync()
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<Long> future = new Future<Long>();
            Backendless.Data.of( Comunidad.class ).remove( this, future );

            return future;
        }
    }

    public void removeAsync( AsyncCallback<Long> callback )
    {
        Backendless.Data.of( Comunidad.class ).remove( this, callback );
    }

    public static Comunidad findById( String id )
    {
        return Backendless.Data.of( Comunidad.class ).findById( id );
    }

    public static Future<Comunidad> findByIdAsync( String id )
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<Comunidad> future = new Future<Comunidad>();
            Backendless.Data.of( Comunidad.class ).findById( id, future );

            return future;
        }
    }

    public static void findByIdAsync( String id, AsyncCallback<Comunidad> callback )
    {
        Backendless.Data.of( Comunidad.class ).findById( id, callback );
    }

    public static Comunidad findFirst()
    {
        return Backendless.Data.of( Comunidad.class ).findFirst();
    }

    public static Future<Comunidad> findFirstAsync()
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<Comunidad> future = new Future<Comunidad>();
            Backendless.Data.of( Comunidad.class ).findFirst( future );

            return future;
        }
    }

    public static void findFirstAsync( AsyncCallback<Comunidad> callback )
    {
        Backendless.Data.of( Comunidad.class ).findFirst( callback );
    }

    public static Comunidad findLast()
    {
        return Backendless.Data.of( Comunidad.class ).findLast();
    }

    public static Future<Comunidad> findLastAsync()
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<Comunidad> future = new Future<Comunidad>();
            Backendless.Data.of( Comunidad.class ).findLast( future );

            return future;
        }
    }

    public static void findLastAsync( AsyncCallback<Comunidad> callback )
    {
        Backendless.Data.of( Comunidad.class ).findLast( callback );
    }

    public static BackendlessCollection<Comunidad> find( BackendlessDataQuery query )
    {
        return Backendless.Data.of( Comunidad.class ).find( query );
    }

    public static Future<BackendlessCollection<Comunidad>> findAsync( BackendlessDataQuery query )
    {
        if( Backendless.isAndroid() )
        {
            throw new UnsupportedOperationException( "Using this method is restricted in Android" );
        }
        else
        {
            Future<BackendlessCollection<Comunidad>> future = new Future<BackendlessCollection<Comunidad>>();
            Backendless.Data.of( Comunidad.class ).find( query, future );

            return future;
        }
    }

    public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Comunidad>> callback )
    {
        Backendless.Data.of( Comunidad.class ).find( query, callback );
    }
}