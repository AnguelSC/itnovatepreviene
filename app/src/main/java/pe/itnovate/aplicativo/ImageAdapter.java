package pe.itnovate.aplicativo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class ImageAdapter extends ArrayAdapter<String>
{
    private LayoutInflater mInflater;
    private final String[] titlelista;
    private final Integer[] iconlista;
    private int mResource;

    /**
     * Constructor
     * @param context     The current context.
     * @param resource    The resource ID for a layout file containing a TextView to use when
     *                    instantiating views.
     * @param titulos
     */
    public ImageAdapter(Context context, int resource, String[] titulos, Integer[] iconlista)
    {
        super( context, resource,titulos);
        mResource = resource;
        this.titlelista = titulos;
        this.iconlista = iconlista;
        mInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    /**
     * {@inheritDoc}
     */
    private static final float GESTURE_THRESHOLD_DP = 16.0f;
    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        View view = convertView == null ? mInflater.inflate( mResource, parent, false ) : convertView;
        TextView textView = (TextView) view.findViewById(R.id.title);
        textView.setText(titlelista[position]);
        textView.setTextColor(Color.parseColor("#ffffff"));
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(iconlista[position]);
        switch (position){
            case 0:
                view.setBackgroundColor(Color.parseColor("#2ecc71"));
                break;
            case 1:
                view.setBackgroundColor(Color.parseColor("#3498db"));
                break;
            case 2:
                view.setBackgroundColor(Color.parseColor("#f1c40f"));
                break;
            case 3:
                view.setBackgroundColor(Color.parseColor("#e74c3c"));
                break;
        }
        return view;
    }
}