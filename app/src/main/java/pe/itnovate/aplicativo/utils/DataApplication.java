package pe.itnovate.aplicativo.utils;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.backendless.BackendlessUser;

import pe.itnovate.aplicativo.Models.Comunidad;
import pe.itnovate.aplicativo.Models.Entity.Community;

/**
 * Created by AnguelSC on 03/11/2015.
 */
public class DataApplication extends MultiDexApplication
{
    private BackendlessUser CurrentUser;
    private Community currentCommunity;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
    public BackendlessUser getCurrentUser() {
        return CurrentUser;
    }

    public void setCurrentUser(BackendlessUser currentUser) {
        CurrentUser = currentUser;
    }

    public Community getCurrentCommunity() {
        return currentCommunity;
    }

    public void setCurrentCommunity(Community currentCommunity) {
        this.currentCommunity = currentCommunity;
    }
}