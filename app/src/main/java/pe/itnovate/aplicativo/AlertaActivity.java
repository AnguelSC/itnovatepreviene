package pe.itnovate.aplicativo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.pushbots.push.Pushbots;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.itnovate.aplicativo.Models.Entity.Member;


public class AlertaActivity extends Activity implements ConnectionCallbacks,
        OnConnectionFailedListener,LocationListener {
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://server-previene-goldensniperos.c9users.io");
        } catch (URISyntaxException e) {
        }
    }
    private static final String TAG = AlertaActivity.class.getSimpleName();

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;


    private Realm realm;
    RealmResults<Member> realmResults;

    private double latitud;
    private double longitud;
    // UI elements

    private ListView tablesListView;
    private boolean status = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getInstance(this);
        setContentView(R.layout.activity_alerta);

        // First we need to check availability of play services
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }

    }

    protected void startLocationUpdates() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, (LocationListener) this);
    }

    private boolean displayLocation() {
        startLocationUpdates();
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitud = mLastLocation.getLatitude();
            longitud = mLastLocation.getLongitude();
            return true;
            //Toast.makeText(getApplicationContext(), latitud + "-" + longitud , Toast.LENGTH_LONG).show();
        } else {
            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
            Toast.makeText(getApplicationContext(),"Sin el GPS esta aplicacion no podra enviar alertas",Toast.LENGTH_LONG).show();
            return false;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        if(displayLocation()){
            final SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
            final pe.itnovate.aplicativo.Models.Alerta alertas = new pe.itnovate.aplicativo.Models.Alerta();
            alertas.setLatitud(latitud);
            alertas.setLongitud(longitud);
            alertas.setNombre(sharedPreferences.getString("name", ""));
            alertas.setEstado(0);
            alertas.setTelefono(sharedPreferences.getString("phone", ""));
            try {
                JSONObject obj=new JSONObject();
                obj.put("latitude",latitud);
                obj.put("longitude",longitud);
                //obj.put("deviceId", Pushbots.sharedInstance().regID());
                obj.put("name",sharedPreferences.getString("name", ""));
                obj.put("phone",sharedPreferences.getString("phone", ""));
                Bundle extras = getIntent().getExtras();
                if(extras != null)
                    if(extras.getString("community") != null) {
                        final String Community= extras.getString("community");
                        JSONArray jsonArray = new JSONArray();
                        RealmResults<Member> check = realm.where(Member.class).equalTo("community.name", Community).findAll();
                        for (Member member : check) {
                            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                            try {
                                Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(member.getNumberPhone(), "PE");
                                jsonArray.put(swissNumberProto.getNationalNumber());
                            } catch (NumberParseException e) {
                                System.err.println("NumberParseException was thrown: " + e.toString());
                            }
                        }
                        obj.put("numbers", jsonArray);
                    }
                obj.put("objectId", alertas.getObjectId());
                mSocket.emit("alerta", obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Backendless.Persistence.save(alertas, new AsyncCallback<pe.itnovate.aplicativo.Models.Alerta>() {
                public void handleResponse(pe.itnovate.aplicativo.Models.Alerta response) {

                }

                public void handleFault(BackendlessFault fault) {
                    // an error has occurred, the error code can be retrieved with fault.getCode()
                }
            });
            Toast.makeText(getApplicationContext(), "TU ALERTA FUE ENVIADA" , Toast.LENGTH_LONG).show();
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                /*Intent myIntent = new Intent(getApplicationContext(), ListActivity.class);
                myIntent.putExtra("objectId", getIntent().getExtras().getString("_listObject"));
                myIntent.putExtra("optionId", getIntent().getExtras().getString("optionId"));
                startActivityForResult(myIntent, 0);*/
                finish();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnectionSuspended(int arg0) {
            mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public void alertar(View v){

    }
}
