package pe.itnovate.aplicativo;

/**
 * Created by AnguelSC on 20/11/2015.
 */
public class MenuOptions {
    private String title;
    private int icon;
    public MenuOptions(String _title,int _icon){
        title = _title;
        icon = _icon;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
