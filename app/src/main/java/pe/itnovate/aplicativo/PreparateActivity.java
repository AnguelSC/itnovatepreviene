package pe.itnovate.aplicativo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pe.itnovate.aplicativo.Models.Video;


public class PreparateActivity extends AppCompatActivity {

    private ListView listadoVideos;
    private ArrayList<Video> videos = new ArrayList<>();

    String[] videitos = {"7dKXekA8ZU4","nDJUPk6iVRQ","BQ47zXQJafs","w5H61HeK8B0"};
    private videosAdapter adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparate);
        videos.add(new Video("Videoclip Educacion y Gestion del Riesgo", R.drawable.video_1));
        videos.add(new Video("¿Cómo ocurre El Niño y La Niña? Video BBC Mundo",R.drawable.video_2));
        videos.add(new Video("Fenómeno del Niño - Prevención Botiquín",R.drawable.video_3));
        videos.add(new Video("Defensa civil toma precauciones ante inminente llegada del fenómeno del niño",R.drawable.video_4));

        adaptador = new videosAdapter(this,R.layout.itemvideolayout,videos);

        listadoVideos = (ListView)findViewById(R.id.listView);
        listadoVideos.setAdapter(adaptador);
        listadoVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PreparateActivity.this,Auxilios.class);
                intent.putExtra("VIDEO_ID",videitos[position]);
                startActivity(intent);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }



}
