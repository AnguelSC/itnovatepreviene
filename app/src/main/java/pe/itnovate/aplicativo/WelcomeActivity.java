package pe.itnovate.aplicativo;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.pushbots.push.Pushbots;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.itnovate.aplicativo.Models.Colegio;
import pe.itnovate.aplicativo.Models.Entity.Community;
import pe.itnovate.aplicativo.Models.Entity.School;
import pe.itnovate.aplicativo.Models.MenuAdapter;
import pe.itnovate.aplicativo.utils.DataApplication;
import pe.itnovate.aplicativo.utils.customHandler;


public class WelcomeActivity extends AppCompatActivity {
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://server-previene-goldensniperos.c9users.io");
        } catch (URISyntaxException e) {
            Toast.makeText(getApplicationContext(),"no contecta socket"+e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }
    static final String[] titles = new String[] {
            "PREPÁRATE","INFÓRMATE","PRIMEROS AUXILIOS","ORGANÍZATE"
    };
    static final List<MenuOptions> menuOptions = new ArrayList<>();
    static final Integer[] images = new Integer[] {
            R.drawable.ic_menu1, R.drawable.ic_menu2, R.drawable.ic_menu3, R.drawable.nose_01
    };
    private Realm realm;
    RealmResults<Community> realmResults;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(menuOptions.size() == 0){
            menuOptions.add(new MenuOptions("PREPÁRATE",R.drawable.ic_menu_1));
            menuOptions.add(new MenuOptions("INFÓRMATE",R.drawable.ic_menu_2));
            menuOptions.add(new MenuOptions("PRIMEROS AUXILIOS",R.drawable.ic_menu_3));
            menuOptions.add(new MenuOptions("ORGANÍZATE",R.drawable.ic_menu_4));
        }
        super.onCreate(savedInstanceState);
        mSocket.connect();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        realm = Realm.getInstance(this);
        String appVersion = "v1";
        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().setCustomHandler(customHandler.class);
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(sharedPreferences.getString("phone", ""), "PE");
            Pushbots.sharedInstance().setAlias(swissNumberProto.getNationalNumber()+"");
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        Backendless.initApp(this, "632B985C-6A06-A963-FFA0-46AD199D8000", "9A4BB63C-F8F3-6CA6-FF98-3E3E4B099400", appVersion);
        //DeviceRegistration devReg = Backendless.Messaging.getRegistrations();
        //Toast.makeText( getApplicationContext(), devReg.getDeviceId(), Toast.LENGTH_SHORT ).show();
        /*Backendless.Messaging.registerDevice("1049025845042", "default", new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Registrado", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });

        Backendless.Messaging.subscribe( "default",
            new AsyncCallback<List<Message>>()
            {
                public void handleResponse( List<Message> response )
                {
                    for( Message message : response )
                    {
                        String publisherId = message.getPublisherId();
                        Object data = message.getData();
                    }
                }
                public void handleFault( BackendlessFault fault )
                {
                    Toast.makeText( getApplicationContext(), fault.getMessage(), Toast.LENGTH_SHORT ).show();                                       }
            },
            new AsyncCallback<Subscription>(){
                public void handleResponse( Subscription response )
                {
                }
                public void handleFault( BackendlessFault fault )
                {
                    Toast.makeText( getApplicationContext(), fault.getMessage(), Toast.LENGTH_SHORT ).show();
                }
            }
        );*/
        /*DeliveryOptions deliveryOptions = new DeliveryOptions();
        deliveryOptions.setPushPolicy( PushPolicyEnum.ONLY );
        deliveryOptions.addPushSinglecast( "LGD2903a24dce" );

        PublishOptions publishOptions = new PublishOptions();
        publishOptions.putHeader( "android-ticker-text", "You just got a private push notification!" );
        publishOptions.putHeader("android-content-title", "This is a notification title");
        publishOptions.putHeader("android-content-text", "Push Notifications are cool");

        MessageStatus status = Backendless.Messaging.publish( "this is a private message!", publishOptions, deliveryOptions );*/
        /*realm.beginTransaction();
        RealmResults<School> result = realm.where(School.class).findAll();
        result.clear();
        realm.commitTransaction();
        Backendless.Data.of(Colegio.class).find(new AsyncCallback<BackendlessCollection<Colegio>>() {
            @Override
            public void handleResponse(BackendlessCollection<Colegio> ColegiosData) {
                while (ColegiosData.getCurrentPage().size() > 0) {
                    for (Colegio colegio : ColegiosData.getCurrentPage()){
                            realm.beginTransaction();
                            School school = realm.createObject(School.class);
                            school.setName(colegio.getNombre().toString());
                            if(colegio.getTelefono() != null)
                            school.setPhone(colegio.getTelefono().toString());
                            school.setLatitude(colegio.getLatitud());
                            school.setLongitude(colegio.getLongitud());
                            if(colegio.getDireccion() != null)
                                school.setStreet(colegio.getDireccion().toString());
                            realm.commitTransaction();
                    }
                    ColegiosData = ColegiosData.nextPage();
                }
                Toast.makeText(getApplicationContext(), "cargado completo", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });*/

        SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
        String DEVICE = sharedPreferences.getString("deviceId", "");
        String name = sharedPreferences.getString("name", "");
        String email = sharedPreferences.getString("email", "");
        String password = sharedPreferences.getString("password", "");
        final DataApplication dataApplication = (DataApplication) getApplication();
        if(name != null && !name.isEmpty()){
                /*try
                {
                    //dataApplication.setCurrentUser(Backendless.UserService.login( email, password ,true));
                }
                catch( BackendlessException exception )
                {
                    // login failed, to get the error code, call exception.getFault().getCode()
                }*/
            setContentView(R.layout.activity_welcome);
            ListView listView = (ListView) findViewById(R.id.listView3);
            ArrayAdapter arrayAdapter = new MenuAdapter(this,R.layout.layout_menu_option,menuOptions);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(WelcomeActivity.this, PreparateActivity.class);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(WelcomeActivity.this, MapsActivity.class);
                            startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(WelcomeActivity.this, Primeros.class);
                            startActivity(intent);
                            break;
                        case 3:
                            intent = new Intent(WelcomeActivity.this, CommunitiesActivity.class);
                            startActivity(intent);
                            break;
                    }
                }
            });

            listView.setAdapter(arrayAdapter);
                 /*       Intent intent;

                        }*/

                /*gridview.setBackgroundColor(Color.WHITE);
                gridview.setVerticalSpacing(10);
                gridview.setHorizontalSpacing(10);
                gridview.setAdapter(adapter);*/
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String numberPhone = extras.getString("numberOrigin");
            if (numberPhone != null){
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + numberPhone));
                startActivity(intent);
            }
        }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(WelcomeActivity.this,SettingsActivity.class));
            return true;
        }
        if(id == R.id.action_salir){
            SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            finish();
            Intent intent = new Intent(this,LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }
        if(id == R.id.sync){
            Intent intent = new Intent(this, WelcomeActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, 0);

            realm.commitTransaction();
            final NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("Sincronizando...").setContentText("Descargando zonas de riesgo")
                            .setContentIntent(pendingIntent)
                            .setOngoing(true)
                            .setAutoCancel(true);

            final NotificationManager notificationManager = (NotificationManager) this
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            realm.beginTransaction();
            RealmResults<Community> check = realm.where(Community.class).findAll();
            check.clear();
            realm.commitTransaction();
            realm.beginTransaction();
            builder.setProgress(0, 0, true);
            notificationManager.notify(0, builder.build());
            Backendless.Data.of(Colegio.class).find(new AsyncCallback<BackendlessCollection<Colegio>>() {
                @Override
                public void handleResponse(BackendlessCollection<Colegio> ColegiosData) {
                    while (ColegiosData.getCurrentPage().size() > 0) {
                        for (Colegio colegio : ColegiosData.getCurrentPage()) {
                            School school = realm.createObject(School.class);
                            school.setName(colegio.getNombre().toString());
                            if (colegio.getTelefono() != null)
                                school.setPhone(colegio.getTelefono().toString());
                            if (colegio.getDireccion() != null)
                                school.setStreet(colegio.getDireccion().toString());
                            school.setLatitude(colegio.getLatitud());
                            school.setLongitude(colegio.getLongitud());
                        }
                        ColegiosData = ColegiosData.nextPage();
                    }
                    builder.setContentText("Descarga completa")
                            .setProgress(0, 0, false);
                    notificationManager.notify(0, builder.build());
                }

                @Override
                public void handleFault(BackendlessFault backendlessFault) {
                    builder.setContentText("Descarga fallida")
                            .setProgress(0, 0, false);
                    notificationManager.notify(0, builder.build());
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    public void awdawd(View v){
        Intent intent = new Intent(this,AlertaActivity.class);
        startActivity(intent);
    }
    public void login(View v){
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}
