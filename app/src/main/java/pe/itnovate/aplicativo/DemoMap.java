package pe.itnovate.aplicativo;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DemoMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String latitude =  extras.getString("latitude");
            String longitude = extras.getString("longitude");
            String radius = extras.getString("radius");
            Double  radio = Double.parseDouble(radius);
            LatLng sydney = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            CircleOptions co = new CircleOptions();
            co.center(sydney);
            co.radius(radio);
            co.fillColor(Color.parseColor("#F44336"));
            co.strokeColor(Color.parseColor("#B71C1C"));
            co.strokeWidth(1);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Lambayeque 4.7"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10.0f));
            mMap.addCircle(co);

        }
    }
}
