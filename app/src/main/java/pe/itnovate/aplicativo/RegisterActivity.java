package pe.itnovate.aplicativo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


public class RegisterActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
    }
    public void registrar(View v){
        EditText editText = (EditText) findViewById(R.id.editText);//telefono
        EditText editText2 = (EditText) findViewById(R.id.editText2);//nombres
        EditText editText3 = (EditText) findViewById(R.id.editText3);//apellidos
        EditText editText4 = (EditText) findViewById(R.id.editText4);//apellidos
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Registrando...");
        progressDialog.show();
        if (editText.getText().toString() != "" &&
                editText2.getText().toString() != "" &&
                editText3.getText().toString() != ""){

            SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            TelephonyManager tMgr = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = tMgr.getDeviceId();
            editor.putString("deviceId", deviceId);
            editor.putString("phone", editText.getText().toString());
            editor.putString("name", editText2.getText().toString());
            editor.putString("email", editText3.getText().toString());
            editor.putString("password", editText4.getText().toString());
            editor.commit();
            BackendlessUser user = new BackendlessUser();
            user.setEmail(editText3.getText().toString());
            user.setProperty("name", editText2.getText().toString());
            user.setProperty("phone", editText.getText().toString());
            user.setPassword(editText4.getText().toString());

            Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
                public void handleResponse(BackendlessUser registeredUser) {
                    Intent intent = new Intent(RegisterActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                    progressDialog.dismiss();
                    Toast.makeText(getApplication(), "Registrado correctamente", Toast.LENGTH_LONG).show();
                    finish();
                }

                public void handleFault(BackendlessFault fault) {
                    Toast.makeText(getApplication(), "Ocurrio un problema", Toast.LENGTH_LONG).show();
                }
            });
        }else{

            Toast.makeText(getApplication(),"Verifica los datos",Toast.LENGTH_LONG).show();
        }
    }

}
