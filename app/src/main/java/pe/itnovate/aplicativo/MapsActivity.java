package pe.itnovate.aplicativo;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.itnovate.aplicativo.Models.Colegio;
import pe.itnovate.aplicativo.Models.Entity.Member;
import pe.itnovate.aplicativo.Models.Entity.School;
import pe.itnovate.aplicativo.Models.Zona;
import pe.itnovate.aplicativo.utils.DataApplication;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Realm realm;

    private Location mLastLocation;

    private GoogleApiClient mGoogleApiClient;
    private double latitud;
    private double longitud;
    List<PolygonOptions> lista = new ArrayList<PolygonOptions>();
    private List<Marker> markersMedicos = new ArrayList<>();
    private List<Marker> markersBomberos = new ArrayList<>();
    private List<Marker> markersPolicias = new ArrayList<>();
    private List<Marker> markersEducativas = new ArrayList<>();
    private MaterialDialog mMaterialDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        DataApplication dataApplication = (DataApplication) getApplication();
        realm = Realm.getInstance(dataApplication.getContext());
        setUpMapIfNeeded();
        if (checkPlayServices()) {

            buildGoogleApiClient();
        }
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group_list_selector);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioNearBy:
                        hideColegios();
                        break;
                    case R.id.radioNearBy_2:
                        hideColegios();
                        break;
                    case R.id.radioNearBy_3:
                        hideColegios();
                        break;
                    case R.id.radioNearBy_4:
                        hideColegios();
                        showColegios();
                        break;
                }
            }
        });

    }

    private void hideColegios(){
        for (Marker m: markersEducativas){
            m.setVisible(false);
        }
    }

    private void showColegios(){
        for (Marker m: markersEducativas){
            m.setVisible(true);
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                // mMap.getUiSettings().setMapToolbarEnabled(true);
                AgregaZonas();
            }
        }
    }

    private void AgregaZonas(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean pref = prefs.getBoolean("network_switch", false);
        if (pref == true){

        }else {
            BackendlessDataQuery query = new BackendlessDataQuery();
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.addRelated("Lugares");
            query.setQueryOptions(queryOptions);
            query.setPageSize(100);
            Backendless.Data.of(Zona.class).find(query, new AsyncCallback<BackendlessCollection<Zona>>() {
                @Override
                public void handleResponse(BackendlessCollection<Zona> zonasBackendlessCollection) {
                    boolean noCorras = true;
                    for (Zona zona : zonasBackendlessCollection.getData()) {
                        PolygonOptions polygonOptions = new PolygonOptions()
                                .strokeWidth(1)
                                .fillColor(Color.parseColor("#80FF0000"));

                        for (int i = 1; i <= zona.getLugares().size(); i++) {
                            for (int j = 0; j < zona.getLugares().size(); j++) {
                                if (i == zona.getLugares().get(j).getOrden()) {
                                    polygonOptions.add(new LatLng(zona.getLugares().get(j).getLatitud(), zona.getLugares().get(j).getLongitud()));
                                }
                            }
                        }
                        lista.add(polygonOptions);
                        mMap.addPolygon(polygonOptions);
                    }

                    for (PolygonOptions poligono : lista) {
                        if (PolyUtil.containsLocation(new LatLng(latitud, longitud), poligono.getPoints(), true)) {
                            Toast.makeText(getApplicationContext(), "Estas en Zona de Riego", Toast.LENGTH_LONG).show();
                            noCorras = false;
                            break;
                        }
                    }
                    if (noCorras) {
                    }
                }

                @Override
                public void handleFault(BackendlessFault backendlessFault) {

                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    protected void startLocationUpdates() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, (LocationListener) this);
    }

    private void displayLocation() {
        startLocationUpdates();
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitud = mLastLocation.getLatitude();
            longitud = mLastLocation.getLongitude();
            LatLng sydney = new LatLng(latitud, longitud);
            LatLng defaultPoint = new LatLng(-9.2821802,-77.6719598);
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean pref = prefs.getBoolean("network_switch", false);
            if(pref == true){
                RealmResults<School> data = realm.where(School.class).findAll();
                Toast.makeText(getApplicationContext(), data.size()+"", Toast.LENGTH_LONG).show();
                for (School school : data){
                    MarkerOptions marker = new MarkerOptions().position(new LatLng(school.getLatitude(), school.getLongitude())).title(school.getName().toString());
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.university));
                    markersEducativas.add(mMap.addMarker(marker));
                }
            }else{
                Backendless.Data.of(Colegio.class).find(new AsyncCallback<BackendlessCollection<Colegio>>() {
                    @Override
                    public void handleResponse(BackendlessCollection<Colegio> ColegiosData) {
                        while (ColegiosData.getCurrentPage().size() > 0) {
                            for (Colegio colegio : ColegiosData.getCurrentPage()){
                                MarkerOptions marker = new MarkerOptions().position(new LatLng(colegio.getLatitud(), colegio.getLongitud())).title(colegio.getNombre().toString());
                                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.university));
                                markersEducativas.add(mMap.addMarker(marker));
                            }
                            ColegiosData = ColegiosData.nextPage();
                        }
                    }
                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {

                    }
                });
            }
        } else {
            mMaterialDialog = new MaterialDialog.Builder(this)
                .title("GPS")
                .content("Itnovate Previene necesita que actives tu GPS para mayor presion en los datos")
                .positiveText("Activar")
                .negativeText("Cancelar")
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (which.name() == "POSITIVE") {
                            finish();
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            Toast.makeText(getApplication(),"Luego de activar volver a ingresar a la opcion INFORMATE",Toast.LENGTH_LONG).show();
                        } else {

                        }
                        mMaterialDialog.dismiss();
                    }
                }).show();
            Toast.makeText(getApplicationContext(), "GPS deshabilitado", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        /*boolean noCorras = true;
        for (PolygonOptions poligono:lista) {
            if(PolyUtil.containsLocation(new LatLng(location.getLatitude(),location.getLongitude()),poligono.getPoints(),true)){
                Toast.makeText(getApplicationContext(), "Estas en Zona de Riego, Corre por tu vida :(", Toast.LENGTH_LONG).show();
                noCorras = false;
                break;
            }
        }
        if(noCorras){
            Toast.makeText(getApplicationContext(), " Estas en tierra santa, puedes caminar tranquilo :)", Toast.LENGTH_LONG).show();
        }*/
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        checkPlayServices();
    }
}
