package pe.itnovate.aplicativo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pe.itnovate.aplicativo.Models.Video;

/**
 * Created by Angel Sirlopu C on 18/10/2015.
 */
public class videosAdapter extends ArrayAdapter<Video>{
    private LayoutInflater mInflater;
    private final ArrayList<Video> videos;
    private int mResource;

    /**
     * Constructor
     * @param context     The current context.
     * @param resource    The resource ID for a layout file containing a TextView to use when
     *                    instantiating views.
     * @param titulos
     */
    public videosAdapter(Context context, int resource, ArrayList<Video> titulos)
    {
        super( context, resource,titulos);
        mResource = resource;
        this.videos = titulos;
        mInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    /**
     * {@inheritDoc}
     */
    private static final float GESTURE_THRESHOLD_DP = 16.0f;
    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        View view = convertView == null ? mInflater.inflate( mResource, parent, false ) : convertView;
        Video item = videos.get(position);
        String nombre = item.getDescripcion();
        ImageView image = (ImageView) view.findViewById(R.id.image_adapter);
        image.setImageResource(item.getImagen());
        /*image.set
        image.setLayoutParams();*/
        TextView titulo = (TextView) view.findViewById( R.id.text_adapter);
        titulo.setText( nombre );
        //titulo.setTextColor(Color);
        return view;
    }

}
