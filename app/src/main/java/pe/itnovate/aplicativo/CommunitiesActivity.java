package pe.itnovate.aplicativo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.itnovate.aplicativo.Models.Entity.Community;
import pe.itnovate.aplicativo.utils.DataApplication;

public class CommunitiesActivity extends AppCompatActivity {
    private ArrayAdapter<String> _adapter;
    private ArrayList<String> myDataset = new ArrayList<>();
    private Realm realm;
    ListView listView ;
    RealmResults<Community> realmResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        realm = Realm.getInstance(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communities);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listView2);
        realmResults = realm.where(Community.class).findAll();
        for (Community _community : realmResults){
            myDataset.add(_community.getName().toString());
        }
        _adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, myDataset);
        listView.setAdapter(_adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DataApplication dataApplication = (DataApplication) getApplication();
                Community community = realmResults.get(position);
                dataApplication.setCurrentCommunity(community);
                startActivity(new Intent(CommunitiesActivity.this, MyCommunityActivity.class));
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(CommunitiesActivity.this)
                        .title("Mis Comunidades")
                        .content("Ingresa el nombre de tu comunidad")
                        .inputType(InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
                                InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .positiveText("Crear")
                        .input("", "", false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                realm.beginTransaction();
                                RealmResults<Community> check = realm.where(Community.class).equalTo("name", input.toString()).findAll();
                                if (check.size() > 0) {
                                    Toast.makeText(getApplication(), "Ya tienes una comunidad creada con ese nombre", Toast.LENGTH_LONG).show();
                                } else {
                                    Community community = realm.createObject(Community.class);
                                    community.setName(input.toString());
                                    Toast.makeText(getApplication(), "Tu Comunidad ha sido creada", Toast.LENGTH_LONG).show();
                                    myDataset.add(input.toString());
                                    _adapter.notifyDataSetChanged();
                                }
                                realm.commitTransaction();
                            }
                        }).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comunidad, menu);
        return super.onCreateOptionsMenu(menu);
        //return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(CommunitiesActivity.this,SettingsActivity.class));
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_salir:
                SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                finish();
                Intent intent = new Intent(this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_borrar:
                new MaterialDialog.Builder(this)
                        .title("Eliminar comunidad")
                        .content("Elimine ingresando el nombre de la comunidad")
                        .inputType(InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
                                InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .positiveText("Eliminar")
                        .input("Nombre de la comunidad", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                realm.beginTransaction();
                                RealmResults<Community> check = realm.where(Community.class).equalTo("name", input.toString()).findAll();
                                if (check.size() > 0) {
                                    check.clear();
                                    Toast.makeText(getApplication(), "Tu Comunidad ha eliminada", Toast.LENGTH_LONG).show();
                                    myDataset.clear();
                                    realmResults = realm.where(Community.class).findAll();
                                    for (Community _community : realmResults) {
                                        myDataset.add(_community.getName().toString());
                                    }
                                    _adapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(getApplication(), "Esa comunidad no existe", Toast.LENGTH_LONG).show();
                                }
                                realm.commitTransaction();
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
