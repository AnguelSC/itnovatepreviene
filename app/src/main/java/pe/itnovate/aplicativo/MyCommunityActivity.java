package pe.itnovate.aplicativo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.itnovate.aplicativo.Models.ContactItem;
import pe.itnovate.aplicativo.Models.Entity.Community;
import pe.itnovate.aplicativo.Models.Entity.Member;
import pe.itnovate.aplicativo.utils.DataApplication;

public class MyCommunityActivity extends AppCompatActivity{
    private Community comunidad;
    private ArrayAdapter<String> _adapter;
    private ArrayList<String> mylist = new ArrayList<String>();
    private ArrayList<ContactItem> listContacts = new ArrayList<>();
    ListView listView ;
    CharSequence[] cs;
    private Realm realm;
    RealmResults<Member> realmResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        realm = Realm.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_community);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final DataApplication dataApplication = (DataApplication) getApplication();
        comunidad = dataApplication.getCurrentCommunity();
        if (comunidad == null) {
            finish();
        } else {
            toolbar.setTitle(comunidad.getName());
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            listView = (ListView) findViewById(R.id.listView2);
            realmResults = realm.where(Member.class).equalTo("community.name", comunidad.getName()).findAll();
            for (Member _community : realmResults) {
                mylist.add(_community.getName().toString());
            }
            _adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, mylist);
            listView.setAdapter(_adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final int awd = position;
                    Snackbar.make(view, "Eliminar miembro ", Snackbar.LENGTH_LONG)
                            .setAction("Eliminar", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    realm.beginTransaction();
                                    RealmResults<Member> check = realm.where(Member.class).equalTo("name", mylist.get(awd).toString()).equalTo("community.name", comunidad.getName()).findAll();
                                    if (check.size() > 0) {
                                        mylist.remove(awd);
                                        check.clear();
                                        _adapter.notifyDataSetChanged();
                                    }
                                    realm.commitTransaction();
                                }
                            }).show();
                }
            });

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final List<String> list = new ArrayList<>();
                    ContentResolver cr = getContentResolver();
                    Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                            null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                    if (cur.getCount() > 0) {
                        while (cur.moveToNext()) {
                            String id = cur.getString(
                                    cur.getColumnIndex(ContactsContract.Contacts._ID));
                            String name = cur.getString(
                                    cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                                ArrayList<String> alContacts = new ArrayList<String>();
                                while (pCur.moveToNext()) {
                                    String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                                    try {
                                        Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(contactNumber, "PE");
                                        alContacts.add(swissNumberProto.getNationalNumber() + "");
                                    } catch (NumberParseException e) {
                                        System.err.println("NumberParseException was thrown: " + e.toString());
                                    }
                                    break;
                                }
                                if (alContacts.size() > 0 && !name.equals(alContacts.get(0)))
                                    list.add(name + " - (" + alContacts.get(0) + ")");
                                listContacts.add(new ContactItem(id, name, alContacts));
                                cs = list.toArray(new CharSequence[list.size()]);
                                pCur.close();
                            }

                        }
                    }
                    MaterialDialog a = new MaterialDialog.Builder(MyCommunityActivity.this)
                            .title("Elije a tu contacto")
                            .items(cs)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                    //showToast(item.getContent().toString());
                                    if (listContacts.get(which).getNumbers().size() > 1) {
                                        Toast.makeText(getApplicationContext(), "Elije un numero principal", Toast.LENGTH_LONG).show();
                                    } else {
                                        realm.beginTransaction();
                                        Member member = realm.createObject(Member.class);
                                        member.setName(text.toString());
                                        member.setCommunity(comunidad);
                                        member.setNumberPhone(listContacts.get(which).getNumbers().get(0).toString());
                                        realm.commitTransaction();
                                    }
                                    mylist.add(text.toString());
                                    _adapter.notifyDataSetChanged();
                                }
                            })
                            .show();
                }
            });

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean pref = prefs.getBoolean("network_switch", false);
        }
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.community_view, menu);
        return super.onCreateOptionsMenu(menu);
        //return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MyCommunityActivity.this,SettingsActivity.class));
            return true;
        }
        if(id == android.R.id.home){
            finish();
            return true;
        }
        if(id == R.id.action_salir){
            SharedPreferences sharedPreferences = getSharedPreferences("myData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            finish();
            Intent intent = new Intent(this,LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }
        if(id == R.id.action_alerta){
            SmsManager smsManager = SmsManager.getDefault();
            String data = "AlertaPrueba";
            realm.beginTransaction();
            RealmResults<Member> check = realm.where(Member.class).equalTo("community.name", comunidad.getName()).findAll();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean pref = prefs.getBoolean("sms_switch", true);
            if (pref && check.size() > 0) {
                for (Member _community : check){
                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                    try {
                        Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(_community.getNumberPhone(), "PE");
                        data = data+","+swissNumberProto.getNationalNumber();
                    } catch (NumberParseException e) {
                        System.err.println("NumberParseException was thrown: " + e.toString());
                    }
                }

                for (Member _community : check){
                    smsManager.sendTextMessage(_community.getNumberPhone(), null, data, null, null);
                }
            }
            realm.commitTransaction();

            Intent intent = new Intent(this,AlertaActivity.class);
            intent.putExtra("community",comunidad.getName());
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
