package pe.itnovate.aplicativo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GeoUser extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_user);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String latitude =  extras.getString("latitude");
            String longitude = extras.getString("longitude");
            //Double radius =  extras.getDouble("radius");
            if (latitude != null && latitude != null){
                //Toast.makeText(getApplicationContext(),radius+"awdawd",Toast.LENGTH_SHORT).show();
                LatLng sydney = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

                CircleOptions co = new CircleOptions();
                co.center(sydney);
                co.radius(2000);
                co.fillColor(Color.parseColor("#F44336"));
                co.strokeColor(Color.parseColor("#B71C1C"));
                mMap.addCircle(co);
                mMap.addMarker(new MarkerOptions().position(sydney).title("Tu amigo se encuentra aqui"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));
            }
        }
    }
}
